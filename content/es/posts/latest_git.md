---
title: "Actualizar git en Linux Mint Tricia"
date: 2020-04-21T11:52:19+02:00
description: Actualiza git para evitar los problemas de seguridad detectados las últimas semanas
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags: 
- linux
- mint
- git
categories:
- Linux
- git
series:
- Tips for linux
image: images/feature2/content.png
---

## Actualización de git en Linux Mint

git ha tenido una oleada de actualizacione la última semana debido a problemas de seguridad.

Para instalar la última versión en Linux Mint Tricia (válido también para Ubuntu 18.04) me he basado en el ppa de los desarrolladores.

``` bash {linenos=false}
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt upgrade
```

Con esto he pasado de git 2.17.1 a la 2.26.2.
