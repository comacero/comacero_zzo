---
author: Hugo Authors
title: Escribir Matemáticas
date: 2019-12-17T12:00:06+09:00
description: Una guía breve para configurar KaTeX
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: Park
authorEmoji: 👽
libraries:
- katex
---

{{< box >}}
Necesitamos la extensión _goldmark katex_, que no tenemos todavía:
[https://github.com/gohugoio/hugo/issues/6544](https://github.com/gohugoio/hugo/issues/6544)
{{< /box >}}

Se pueden habilitar notaciones matemáticas en un proyecto Hugo utilizando librerías JavaScript de terceros.
<!--more-->

En este ejemplo usaremos [KaTeX](https://katex.org/)

- Create un `partial` en el directorio: `/layouts/partials/math.html`
- Dentro de este `partial` metemos la referencia a [Auto-render Extension](https://katex.org/docs/autorender.html) o alojar estos scripts localmente.
- Se incluye el `partial` en tu contenido de la siguiente forma:

```
{{ if or .Params.math .Site.Params.math }}
{{ partial "math.html" . }}
{{ end }}
```
- Para habilitar KaTex globlamente hay que añadir el parámetro `math` a `true` en la configuración del proyecto
- Para habilitar KaTex por página es necesario incluir el parámetro `math: true` en los ficheros de contenido

**Nota:** Consulta la referencia online [Supported TeX Functions](https://katex.org/docs/supported.html)

### Ejemplos

Inline math: $$ \varphi = \dfrac{1+\sqrt5}{2}= 1.6180339887… $$

Block math:

$$
 \varphi = 1+\frac{1} {1+\frac{1} {1+\frac{1} {1+\cdots} } }
$$
