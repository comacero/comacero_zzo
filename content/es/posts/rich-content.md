---
title: "Contenido enriquecido (shortcodes para enlazar media)"
date: 2019-12-19T12:00:06+09:00
description: "Una breve descripción de los códigos cortos (shortcodes) de Hugo"
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: Lee
authorEmoji: 👺
tags:
- shortcodes
- privacy
image: images/feature2/content.png
---

Hugo trae de serie varios [Built-in Shortcodes](https://gohugo.io/content-management/shortcodes/#use-hugo-s-built-in-shortcodes) para contenido enriquecido, junto con una [Configuración de Privacidad](https://gohugo.io/about/hugo-and-gdpr/) y un conjunto de códigos cortos simples (Simple Shortcodes) que habilita versiones estaticas y libres de JS de inserciones desde varias redes sociales.
<!--more-->
---

## Instagram Simple Shortcode


---

## YouTube Privacy Enhanced Shortcode

{{< youtube ZJthWmvUzzc >}}

<br>

---

## Twitter Simple Shortcode

{{< twitter_simple 1085870671291310081 >}}

<br>

---

## Vimeo Simple Shortcode

{{< vimeo_simple 48912912 >}}
