---
title: "Soporte para Katex"
date: 2019-11-15T12:00:06+09:00
description: "KaTeX es una biblioteca JavaScript rápida y fácil de usar para renderizar matemáticas en la web."
draft: false
enableToc: false
enableTocContent: false
tags:
- 
series:
-
categories:
- math
libraries:
- katex
image: images/feature2/mathbook.png
---

Una integral:

$$ \int_{a}^{b} x^2 dx $$

Una fórmula recursiva:

$$ \varphi = 1+\frac{1} {1+\frac{1} {1+\frac{1} {1+\cdots} } } $$

Habilita Katex en el fichero de configuración fijando el parámetro `katex` a `true`. Esto hará que se importen los módulos Katex CSS/JS necesarios. 

Ver la referencia online de [las funciones TeX soportadas](https://katex.org/docs/supported.html). 

**Note:** Para renderizar matemáticas _inline_ correctamente, la extensión del fichero de contenido debe ser `.mmark`. Ver [el sitio oficial](https://mmark.nl/). 

```
Matemáticas inline: $ \varphi = \dfrac{1+\sqrt5}{2}= 1.6180339887… $
```

Matemáticas _inline_: $ \varphi = \dfrac{1+\sqrt5}{2}= 1.6180339887… $

```
Bloques matemáticos:

$$ \varphi = 1+\frac{1} {1+\frac{1} {1+\frac{1} {1+\cdots} } } $$
```

Bloques matemáticos:

$$ \varphi = 1+\frac{1} {1+\frac{1} {1+\frac{1} {1+\cdots} } } $$
