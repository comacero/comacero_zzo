---
title: "Soporte para MathJax"
date: 2019-11-16T12:00:06+09:00
description: "Un motor JavaScript para renderizar matemáticas que funciona en todos los navegadores. Sin configuraciones especiales. Simplemente funciona."
draft: false
enableToc: false
enableTocContent: false
tags:
- 
series:
-
categories:
- math
libraries:
- mathjax
image: images/feature1/infinity.png
---

Cuando $a \ne 0$, hay dos soluciones para $\(ax^2 + bx + c = 0\)\$ y son $$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$
