---
title: "Configuración de Emacs paso a paso (03)"
date: 2020-06-08
description: Paquetes generales para programar
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags:
- linux
- emacs
- org-mode
categories:
- Linux
- Emacs
series:
- Emacs configuration
image: images/feature2/content.png
slug: emacs_conf_03
---

Continuamos configurando nuestro _Emacs_, esta vez vamos a ver paquetes útiles para programar.

Tengo que comentar que uso _Emacs_ como cliente la mayor parte del tiempo. Eso hace que _Emacs_ se arranque una sola vez (por sesión) como servidor y las sucesivas llamadas a _Emacs_ abren buffers en la misma instancia.

Para trabajar de esta forma he creado alias para invocar el editor. En _zsh_ es fácil sin más que instalar el _bundle_ `emacs` de _oh-my_zsh_, que añade alguna funcionalidad a mayores. Pero si no usas zsh ni _oh-my-zsh_ te valdría con estos alias:

```shell
alias ef='emacsclient -create-frame --alternate-editor=""'
alias e='emacsclient --alternate-editor=""'
alias gemacs='/usr/bin/emacs'
```
Se trata de tener alias que llamen siempre a _emacsclient_, en caso de necesidad tenemos un alias que llama al _Emacs_ original.

Trabajar de esa forma es mucho más rápido y carga menos al sistema pero tiendes a acumular un montón de ficheros abiertos (_buffers_ en _Emacs).

Mi atajo `C-del`, o sea control-suprimir para cerrar los _buffers_ y el uso del paquete _projectile_ (`C-c p k` cierra todos los _buffers_ del proyecto activo) me ayudan a mantener acotado el número de ficheros abiertos.

## projectile

_projectile_ identifica un arbol de directorio como proyecto si es un repo de git (considera también otros sistemas de control de versiones, pero yo sólo uso git) o si creamos un fichero _.projectile_ (puede estar vacío)

Con el uso normal que hagamos de nuestro editor _projectile_ va recopilando "proyectos" y los apunta en un fichero en el directorio `~/.emacs.d`

Con la configuración propuesta tenemos muchísimos atajos de teclado para invocar a _projectile_, en [la página de _counsel-projectile](https://github.com/ericdanan/counsel-projectile#getting-started) podemos ver un amplio listado.

Si invocamos `C-c p` gracias a _which-key_ veremos los comandos de _projectile_ a nuestra disposición. Los que más uso son:

`C-c p p`: Cambiar de proyecto
`C-c p f`: Abrir fichero en el proyecto actual
`C-c p b`: Ir a un _buffer_ (un fichero ya abierto en _Emacs_) del proyecto actual
`C-c p k`: Cerrar todos los _buffers_ del proyecto actual.

Durante mi sesión de trabajo suelo cambiar a un proyecto `C-c p p` y abrir los ficheros necesarios en el proyecto que esté trabajando `C-c p f` y terminar cerrando todos los ficheros del proyecto al final `C-c p k`

``` myinit.org
* projectile
#+begin_src emacs-lisp
  (use-package projectile
    :ensure t
    :config
    (projectile-global-mode)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
    (setq projectile-completion-system 'ivy)
    )

  (use-package counsel-projectile
    :ensure t
    :config
    (setq counsel-projectile-mode t)
    )
#+end_src
```

## git

Dos paquetes para interactuar con _git_: _Magit_ y _git-gutter_

_Magit_ es un super paquete, con una versatilidad y utilidad a la altura del _org-mode_, por si solo ya justifica usar _Emacs_. En el artículo [_A walkthrought the Magit interface_](https://emacsair.me/2017/09/01/magit-walk-through/) tenemos una excelente introducción al uso de este paquete que nos permite hacer todas las operaciones de git sin salir de nuestro editor. Pero no solo eso, _magit_ también hará que si usamos _Emacs_ como editor configurado en git se use toda la potencia de _Magit_ al editar los mensajes de commits. `C-c g` Nos llevará a la ventana de estado de _Magit_

Por su parte _git-gutter_ marcará las lineas que estamos editando en cualquier fichero que esté monitorizado por git, como si tuviéramos activado un diff en tiempo real, además con el arbol _hydra_ configurado podemos navegar por los cambios hechos en el fichero y hacer _commits_ de cambios atómicos(pasar al _stage_ solo los cambios en el fichero que nos interesen) `M-g M-g` abre la _hydra_ de _git-gutter_

``` myinit.org
* git
#+begin_src emacs-lisp
  (use-package magit
    :ensure t
    :init
    (progn
      (bind-key "C-x g" 'magit-status)
      )
    )

  (setq magit-status-margin
        '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 18))

  (use-package git-gutter
    :ensure t
    :init
    (global-git-gutter-mode +1)
    )

  (global-set-key (kbd "M-g M-g") 'hydra-git-gutter/body)

  (use-package git-timemachine
    :ensure t
    )

  (defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1)
                              :hint nil)
    "
    Git gutter:
      _j_: next hunk        _s_tage hunk     _q_uit
      _k_: previous hunk    _r_evert hunk    _Q_uit and deactivate git-gutter
      ^ ^                   _p_opup hunk
      _h_: first hunk
      _l_: last hunk        set start _R_evision
    "
    ("j" git-gutter:next-hunk)
    ("k" git-gutter:previous-hunk)
    ("h" (progn (goto-char (point-min))
                (git-gutter:next-hunk 1)))
    ("l" (progn (goto-char (point-min))
                (git-gutter:previous-hunk 1)))
    ("s" git-gutter:stage-hunk)
    ("r" git-gutter:revert-hunk)
    ("p" git-gutter:popup-hunk)
    ("R" git-gutter:set-start-revision)
    ("q" nil :color blue)
    ("Q" (progn (git-gutter-mode -1)
                ;; git-gutter-fringe doesn't seem to
                ;; clear the markup right away
                (sit-for 0.1)
                (git-gutter:clear))
     :color blue))
#+end_src
```

## yasnippet

_yasnippet_ nos permite usar plantillas para bloques muy usados. Por ejemplo si estamos programando, digamos en python, podemos tener plantillas para los bloques _for_ o para un bloque _if-else_  o para la cabecera de ficheros, etc. Junto con _yasnippet_ he instalado _yasnippets-snippets_ que es una buena colección de _snippets_ predefinidos.

Puedes crear fácilmente tus propios _snippets_ (echa un ojo a la documentación)

Como complemento instalo también el paquete _auto-yasnippet_ que permite crear un _snippet_ "al vuelo" mientras estás editando.

```myinit.org
* yasnippet
Place your own snippets on ~/.emacs.d/snippets
  #+begin_src emacs-lisp
    (use-package yasnippet
      :ensure t
      :config
      :init
      (yas-global-mode 1)
      )
    (use-package auto-yasnippet
      :ensure t
      )
    (use-package yasnippet-snippets
      :ensure t)
  #+end_src
```

A mayores añadimos un par de atajos de teclado para definir un _auto-snippet_ y para expandirlo. He asignado la combinación `C-ñ` como prefijo para mi _own-map_ pero podeis asignar cualquier combinación libre, como por ejemplo `C-z`

## flycheck

_flycheck_ nos dará detección de errores sintácticos cuando estamos programando.

```myinit.org
* flycheck
  #+begin_src emacs-lisp
    (use-package flycheck
      :ensure t
      :config
      (global-set-key  (kbd "C-c C-p") 'flycheck-previous-error)
      (global-set-key  (kbd "C-c C-n") 'flycheck-next-error)
      :init
      (global-flycheck-mode t)
      )
  #+end_src
```

## auto-complete o company

Hay dos escuelas principales de auto completado en _Emacs_: podemos usar el paquete _auto-complete_ o el paquete _company_. Yo de momento estoy usando _auto-complete_, pero estoy probando _company_ que parece que da menos problemas de compatibilidad con los paquetes que uso para programar en python.

Los ficheros de configuración basados en _auto-complete_ quedan en el repositorio de gitlab (ver sección [ficheros para descargar](#ficheros-para-descargar)) en la rama _**auto-complete**_. Los he usado durante varios meses sin demasiados problemas. Para el resto de esta serie vamos a utilizar una configuración basada en _company_.

# company

_company_ es un paquete muy modular que se complementa con _plugins para distintas tareas, a medida que vayamos añadiendo nuevas secciones para distintos entornos de programación iremos añadiendo los _plugins_ necesarios.

De momento vamos a añadir la configuración básica de _company_:

```myinit.org
* company
  Autocompletion with company
  #+begin_src emacs-lisp
    (use-package company
      :ensure t
      :config
      (setq company-idle-delay 0)
      (setq company-minimum-prefix-length 3)
      (global-company-mode t)
      )
  #+end_src

  ** company-quickhelp
   In case you want popup help instead of status line help
  #+begin_src emacs-lisp
    (use-package company-quickhelp
      :ensure t
      :config
      (company-quickhelp-mode)
    )
  #+end_src
```

## Ficheros para descargar

Los ficheros descritos: `init.el` y `myinit.org` puedes descargarlos desde [este enlace](https://gitlab.com/comacero/emacs_conf_step/-/archive/master/emacs_conf_step-master.zip)

Los ficheros están almacenados en <https://gitlab.com/comacero/emacs_conf_step> e irán ampliándose a medida que publiquemos esta serie. En realidad el único fichero que tienes que actualizar es el `myinit.org`. El `init.el` debería cambiar automaticamente sin intervención de nadie.

## Referencias

* [C'est la Z: Using Emacs Series](https://cestlaz.github.io/stories/emacs/)
* [Uncle Dave Emacs videolist](https://www.youtube.com/watch?v=d6iY_1aMzeg&list=PLX2044Ew-UVVv31a0-Qn3dA6Sd_-NyA1n)
* [Planet Emacs Life](https://planet.emacslife.com/)
* [use-package](https://github.com/jwiegley/use-package)
* [org-mode quickstart](https://orgmode.org/worg/org-tutorials/org4beginners.html)
* [ivy-counsel-swipper](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html)
* [emacs and python](https://github.com/howardabrams/dot-files/blob/master/emacs-python.org)
* [Yasnippet documentation](http://joaotavora.github.io/yasnippet/)
* [Magit](https://magit.vc/)
