---
title: "Soporte para Flowchart"
date: 2019-11-14T12:00:06+09:00
description: "flowchart.js es una biblioteca de renderizado de gráficas de flujo DSL y SVG que se ejecuta en el navegador y en el terminal
Nodos y conexiones se definen por separado de forma que los nodos se puede reutilizar y las conexiones se pueden cambiar rápidamente."
draft: false
enableToc: false
enableTocContent: false
tags:
-
series:
-
categories:
- diagram
libraries:
- flowchartjs
image: images/feature1/flowchart.png
---

```flowchart
st=>start: Start|past:>http://www.google.com[blank]
e=>end: End|future:>http://www.google.com
op1=>operation: My Operation|past
op2=>operation: Stuff|current
sub1=>subroutine: My Subroutine|invalid
cond=>condition: Yes
or No?|approved:>http://www.google.com
c2=>condition: Good idea|rejected
io=>inputoutput: catch something...|future

st->op1(right)->cond
cond(yes, right)->c2
cond(no)->sub1(left)->op1
c2(yes)->io->e
c2(no)->op2->e
```
