---
author: "Hugo Authors"
title: "Soporte para Emoji"
date: 2019-12-16T12:00:06+09:00
description: "Guia del uso de emoji en Hugo"
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: Kim
authorEmoji: 👻
tags: 
- emoji
- gamoji
- namoji
- bamoji
- amoji
---

Se pueden habilitar los emoji de varias maneras.
<!--more-->
La función [`emojify`](https://gohugo.io/functions/emojify/) puede llamarse directamente en plantillas (templates) o mediante [Shortcodes inlines](https://gohugo.io/templates/shortcode-templates/#inline-shortcodes). 

Para habilitar emoji globalmente, hay que fijar `enableEmoji` a `true` en el fichero de [configuración](https://gohugo.io/getting-started/configuration/) en tu site, y podrás escribir códigos cortos de emoji directamente en los ficheros de contenido; p. ej.


<p><span class="nowrap"><span class="emojify">🙈</span> <code>:see_no_evil:</code></span>  <span class="nowrap"><span class="emojify">🙉</span> <code>:hear_no_evil:</code></span>  <span class="nowrap"><span class="emojify">🙊</span> <code>:speak_no_evil:</code></span></p>
<br>

La [relación de códigos Emoji](http://www.emoji-cheat-sheet.com/) es una referencia muy útil de los códigos cortos de los emoji.

***

**N.B.** Los pasos descritos habilitan emoji del juego de caracteres Estándar Unicode en Hugo, sin embargo el renderizado de estos glifos dependerá del navegador utilizado y de la plataforma. Para cambiar el estilo de los emoji puedes usar emojis de terceros o apilar fuentes; p.ej.

{{< highlight html >}}
.emoji {
font-family: Apple Color Emoji,Segoe UI Emoji,NotoColorEmoji,Segoe UI Symbol,Android Emoji,EmojiSymbols;
}
{{< /highlight >}}

{{< css.inline >}}
<style>
.emojify {
	font-family: Apple Color Emoji,Segoe UI Emoji,NotoColorEmoji,Segoe UI Symbol,Android Emoji,EmojiSymbols;
	font-size: 2rem;
	vertical-align: middle;
}
@media screen and (max-width:650px) {
    .nowrap {
	display: block;
	margin: 25px 0;
}
}
</style>
{{< /css.inline >}}
