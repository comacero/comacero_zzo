---
title: Guía de Sintaxis de Markdown
date: 2019-12-20T12:00:06+09:00
description: Traducción del artículo que muestra la sintáxis básica de Markdown y el formato de elementos HTML.
draft: false
hideToc: false
enableToc: true
enableTocContent: true
author: Choi
authorEmoji: 🤖
tags:
- markdown
- css
- html
- themes
categories:
- themes
- syntax
series:
- Themes Guide
image: images/feature1/markdown.png
---

Este artículo ofrece una muestra de la sintaxis básica de Markdown que puede usarse para crear ficheros de contenido en Hugo, también muestra como se decoran los elementos HTML en un tema de Hugo.

<!--more-->

## Headings

Los siguientes elementos HTML `<h1>`—`<h6>` representan seis niveles de cabeceras de sección. `<h1>` es la sección de mayor nivel mientras que `<h6>` es la menor.

# H1
## H2
### H3
#### H4
##### H5
###### H6

## Párrafo

Xerum, quo qui aut unt expliquam qui dolut labo. Aque venitatiusda cum, voluptionse latur sitiae dolessi aut parist aut dollo enim qui voluptate ma dolestendit peritin re plis aut quas inctum laceat est volestemque commosa as cus endigna tectur, offic to cor sequas etum rerum idem sintibus eiur? Quianimin porecus evelectur, cum que nis nust voloribus ratem aut omnimi, sitatur? Quiatem. Nam, omnis sum am facea corem alique molestrunt et eos evelece arcillit ut aut eos eos nus, sin conecerem erum fuga. Ri oditatquam, ad quibus unda veliamenimin cusam et facea ipsamus es exerum sitate dolores editium rerore eost, temped molorro ratiae volorro te reribus dolorer sperchicium faceata tiustia prat.

Itatur? Quiatae cullecum rem ent aut odis in re eossequodi nonsequ idebis ne sapicia is sinveli squiatum, core et que aut hariosam ex
eat.

## Blockquotes

El elemento `blockquote` representa contenido que se cita desde otra fuente, opcionalmente con una citación que debe figurar dentro de un elemento `footer` o `cite`, y opcionalmente con cambios "in-line" tales como anotaciones o abreviaturas.

#### Blockquote sin atribución

> Tiam, ad mint andaepu dandae nostion secatur sequo quae.
> **Note** that you can use *Markdown syntax* within a blockquote.

#### Blockquote con atribución

> Don't communicate by sharing memory, share memory by communicating.</p>
> — <cite>Rob Pike[^1]</cite>


[^1]: La cita está tomada de una [charla](https://www.youtube.com/watch?v=PAAkCSZUG1c) Rob Pike durante el Gopherfest, el 18 de noviembre de 2015.

## Tablas

Las tablas no forman parte de la espacificación de Markdown, pero Hugo las soporta de serie.

   Name | Age
--------|------
    Bob | 27
  Alice | 23

#### Inline Markdown en las tablas

| Inline&nbsp;&nbsp;&nbsp;     | Markdown&nbsp;&nbsp;&nbsp;  | In&nbsp;&nbsp;&nbsp;                | Table      |
| ---------- | --------- | ----------------- | ---------- |
| *italics*  | **bold**  | ~~strikethrough~~&nbsp;&nbsp;&nbsp; | `code`     |

## Bloques de código

#### Bloques de código con comillas inversas

```
html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
```
#### Bloque de código con sangrado de cuatro espacios

    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Example HTML5 Document</title>
    </head>
    <body>
      <p>Test</p>
    </body>
    </html>

#### Bloque de código con el resaltado de sintáxix interno de Hugo
{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example HTML5 Document</title>
</head>
<body>
  <p>Test</p>
</body>
</html>
{{< /highlight >}}

## Tipos de listas

#### Lista Ordenada

1. Primer item
2. Segundo item
3. Tercer item

#### Lista Desordenada

* List item
* Another item
* And another item

#### Listas Anidadas

* Item
1. First Sub-item
2. Second Sub-item

## Other Elements — abbr, sub, sup, kbd, mark

<abbr title="Graphics Interchange Format">GIF</abbr> is a bitmap image format.

H<sub>2</sub>O

X<sup>n</sup> + Y<sup>n</sup>: Z<sup>n</sup>

Press <kbd><kbd>CTRL</kbd>+<kbd>ALT</kbd>+<kbd>Delete</kbd></kbd> to end the session.

Most <mark>salamanders</mark> are nocturnal, and hunt for insects, worms, and other small creatures.

