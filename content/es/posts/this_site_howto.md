---
title: "Como se hizo este blog"
date: 2020-04-17T11:52:19+02:00
description: Explicación de como se puede montar un site como este con Hugo y Gitlab pages
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags: 
- hugo
- gitlab
- static_site
categories:
- Linux
- Guias
series:
- Guías Linux
image: images/feature2/content.png
libraries:
- mermaid
---

La receta para montar un blog como este

<!-- more -->

Hace años que soy consciente de la existencia de las _Github Pages_ y/o las _Gitlab Pages_ Las probé en su momento [Github](https://github.com/) pero no volví a prestarles atención por falta de tiempo.

También se desde hace mucho tiempo que hay generadores de sitios web estáticos, en su dia hice algunas pruebas con [Pelican](https://github.com/getpelican/pelican) y de [Jekill](https://github.com/jekyll/jekyll) pero no llegué a utilizarlos para nada funcional.

Pero ahora tengo tiempo y sentía curiosidad por el tema, así que he investigado un poco como podría empezar rápido.

## Hugo: el generador de sitios estáticos

Un generador de sitios estáticos es un software que a partir de unos ficheros de contenido, y unos ficheros de temas genera un sitio estático completo. Es decir un sitio web compuesto de htlm y css, nada más. 

Los ficheros de contenido suelen estar escritos en alguna variedad de markdown, org-mode, o algún lenguaje de marcas por el estilo.

Los temas se suelen encontrar en grandes cantidades en la red, y escritos con distintos objetivos. Hay temas para hacer tu página de presentación personal y dar a conocer tu curriculum, temas para blogs, para galerias de fotos, para publicar documentación, etc. etc.

Hay gran cantidad de generadores de sitios estáticos. Tras investigar un poco (muy poco, la verdad) limité mis opciones a dos generadores: [Jekill](https://jekyllrb.com/) y [Hugo](https://gohugo.io/se). Tengo la sensación (totalmente subjetiva) de que Jekill es más potente e intuyo que será más complicado de usar así que he decidido empezar por Hugo.

Hugo parece sencillo de instalar y de usar y no me ha dado pegas hasta ahora.

Aquí tienes [una serie de videos](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) cortitos donde se explican todos los detalles de Hugo si te interesan.

### Instalación de Hugo

Mi instalación es completamente espartana, me he descargado el paquete binario en su última versión (0.69.0 cuando escribo esto) desde [aquí](https://github.com/gohugoio/hugo/releases) Y he dejado el ejecutable `Hugo` en mi directorio `~/bin`. Nada más, ese directorio ya está en mi `PATH` así que con eso basta para mi. 

{{< notice warning >}}
**Ojo** la primera vez instale el paquete binario _Hugo_ para Linux 64 bit, pero más tarde lo tuve que cambiar por el paquete **Hugo_extended**
{{< /notice >}}

Una vez instalado Hugo, no está de más hacer [el tutorial](https://gohugo.io/getting-started/quick-start/)

## Temas para Hugo: Zdoc y Zzo

Después de hacer el tutorial y algunas pruebas me di una vuelta por [la página de temas de Hugo](https://themes.gohugo.io/). Me interesaba buscar un tema que fuera adecuado para publicar guías algo extensas y que soportara publicar en varios idiomas. Al final escogí el tema [Zdoc](https://themes.gohugo.io/hugo-theme-zdoc/) por que cumplía mis requisitos, parecía muy bien documentado y con un aspecto gráfico que me gustaba. Pero en un momento dado entré en [la página del autor del tema](https://zzodocs.netlify.app) y casí sin darme cuenta me instalé el tema [zzo](https://github.com/zzossig/hugo-theme-zzo) que tampoco está nada mal, y parece más completo incluso que el anterior (con galerias de fotos, reseñas de libros, etc) así que continué con este. De todas formas, sigue pendiente probar el tema Zdoc para algúna guía larga y ver como queda.

Hay que agradecerle a [Zzossig](https://github.com/zzossig) el currazo que se ha metido creando estos temas.

### Creación de un site e instalación del tema

Para usar un tema en nuestro site lo más práctico es hacer un fork del tema original. Por un lado podremos mandarle al autor nuestras contribuciones (en mi caso traducciones nada más) por si le interesa incorporarlas a la distribución oficial del tema. Y por otro lado,  si hacemos otras modificaciones del tema las tendremos controladas en nuestro fork.

Así que:

1) Creamos un fork del tema original (en github en mi caso)

2) Creamos un directorio en nuestro ordenador, para todos los ficheros de nuestro sitio estático, de aquí en adelante voy a suponer que nuestro sitio estático se va a llamar "comacero", puedes llamar al directorio como quieras pero yo le voy a llamar simplemente `blog`.

````bash:
    mkdir blog
    cd blog
````

3) Clonamos nuestro fork del tema elegido en el directorio `blog`, aquí haremos las modificaciones del tema (__ajusta la url a tu fork__) 

```bash
git clone https://github.com/zzossig/hugo-theme-zzo
```

4) Creamos el nuevo sitio localmente en nuestro ordenador con Hugo. Lo creamos dentro del  directorio `blog`. Puedes llamarlo como quieras, yo lo voy a llamar igual que el sitio que queremos crear (`comacero` para este ejemplo):

```bash
mkdir comacero
cd comacero
hugo new site . 

ls
archetypes  config.toml  content  data  layouts  static  themes
```

Ya tenemos el esqueleto de nuestro sitio estático, Hugo se ha encargado de poblarlo con directorios.

Para usarlo en Gitlab nuestro sitio tiene que ser un repositorio git:

```bash
git init
echo public > .gitignore
git add .gitignore
git commit -m "First commit"
```

{{< notice info >}} 
Cuando tengamos todo preparado y generemos nuestro sitio en local con Hugo, todo el sitio estático se creará en el directorio `comacero/public`. Pero __no queremos__ que ese directorio suba a Gitlab, es solo para nuestra "vista-previa" del site en local. Por eso lo añadimos al fichero `.gitignore`
{{< /notice >}}

Tenemos que añadir el tema, así que clonamos **nuestro fork** en la ruta `themes/zzo`

{{< notice warning >}} 
Los _Gitlab Runners_ que son los procesos virtuales que van a generar nuestro sitio en Gitlab, __no pueden__ clonar un repo por ssh. Así que al añadir el tema tienes que hacerlo con un site `https://` o incluso clonar directamente el directorio.
{{< /notice >}}


```bash
git submodule add https://github.com/zzossig/hugo-theme-zzo themes/zzo
git submodule update --remote --merge
```

Alternativamente **si tenemos nuestro site y nuestro fork del tema** en el mismo directorio `blog` podemos hacer referencia al submódulo con una ruta relativa:

```bash
git submodule add ../hugo-theme-zzo themes/zzo
git submodule update --remote --merge
```

Pero si optamos por la segunda opción tenemos que ser cuidadosos al subir todo a Gitlab (lo explicamos luego)

Una vez que tenemos nuestro tema descargado podemos copiar la configuración completa del site de ejemplo que viene con el tema. Desde el directorio donde tenemos nuestro site creado por Hugo, copiamos los ficheros de configuración y contenido del site de ejemplo en el site que hemos creado (que está vacío):

```bash
rm config.toml
cp -r themes/zzo/exampleSite/* .
```

Y ya tenemos todo listo para probar, arrancamos el hugo como servidor web:

```bash
hugo -D server
```

Si todo va bien tendríamos que poder conectarnos a `http://localhost:1313` y ver el site de ejemplo.

#### Configuración del site

Con el site funcionando podemos retocar los ficheros de configuración `config.toml` y `params.toml` que están en el directorio `config/_default`. Tendremos que adaptar la configuración de idiomas, nombre del sitio generado, etc. etc. en la página web de zzo tenemos instrucciones bien detalladas.

## Integración con Gitlab Pages

Como ya comenté, había mirado las páginas de Github y de Gitlab (y también las de Bitbucket) hace algunos años. Incluso había probado algún generador de sitios estáticos para temas del trabajo. Me quedé con ideas preconcebidas (y equivocadas): yo pensaba que toda la generación del sitio la hacías en local y después subias a Gitlab el sitio generado. Es decir, yo pensaba que después de ejecutar Hugo para generar todo el sitio estático en el directorio `public` subias ese contenido generado a Gitlab. 

No es así para nada.

En Gitlab la generación de sitios estáticos está completamente integrada con el software Gitlab, lo que permite hacer [integración contínua](https://es.wikipedia.org/wiki/Integraci%C3%B3n_continua). Esta es otra asignatura que tengo pendiente así que me vino muy bien para ir aprendiendo. :smile:

{{< notice info >}} 
Si quieres aprender como va el despliegue de sitios estáticos con _Gitlab Pages_ es muy recomendable que leas [la documentación de las Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) y que para empezar [crees un site basado en la plantilla html/plain](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_bundled_template.html) (un template predefinido).
{{< /notice >}} 

Los siguientes pasos serán:

* Crear nuestro proyecto en Gitlab para subir nuestros ficheros del sitio
* Subir todo menos el contenido del directorio `public`
* Crear el fichero que le especifica al _runner_ que operaciones tiene que hacer para crear el sitio web

### Crear nuestro proyecto en Gitlab

En el disco duro yo  tengo una estructura de directorios tal que así:

```mermaid
graph TD;
  blog --> theme_zzo;
  blog --> comacero;
```

* En el directorio `comacero` tenemos nuestro site
* En el directorio `theme_zzo` tenemos el fork del tema original con nuestras modificaciones.

En Gitlab he creado una estructura parecida:

* Un grupo que se llama `comacero`
* Un proyecto `comacero.gitlab.io` dentro del grupo `comacero` que almacenará el sitio estático, ligado al directorio `blog/comacero` de mi disco duro
* Un proyecto `zzo_hugo_theme` que almacena mi fork del tema original, ligado la directorio `blog/theme_zzo` de mi disco duro

{{< notice info >}} 
Conviene leerse la [documentación de Gitlab](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) con respecto a los nombres de dominio y su correspondencia con los nombres de proyecto.
{{< /notice >}} 

### Subimos nuestro site a Gitlab

En el repo del directorio `blog/comacero` añadimos la url del remoto en Gitlab (`comacero.gitlab.io`) con `git remote set-url`
En el repo del directorio `blog/theme_zzo` tenemos el fork del tema que apunta a un repo de github. Añadimos un remoto adicional apuntando a la url de Gitlab (`zzo_hugo_theme`)
Hacemos un push desde nuestros repos locales hacia Gitlab y ya tenemos los repos preparados.


Para que nuestro repo genere las páginas web tenemos que incluir un fichero con las ordenes que va a ejecutar el _Gitlab runner_.

### Activar el _runner_ de Gitlab

Tenemos que crear en la raiz de nuestro repo `comacero.gitlab.io` un fichero `.gitlab-ci.yml` con el siguiente contenido:

```.gitlab-ci.yml
# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
# image: registry.gitlab.com/pages/hugo:latest
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

{{< notice warning >}} 
Mucho ojo, porque para nuestro tema necesitamos usar hugo-extended y no es fácil encontrar la imagen correcta en Gitlab. Afortunadamente había hecho pruebas con el template que nos da Gitlab para Hugo, y si os fijais en el comentario de la cabecera nos indica en que URL están todas las imágenes disponibles de Hugo.
{{< /notice >}} 

## Final

Siguiendo estos pasos deberíamos tener nuestro site disponible en la dirección https://comacero.gitlab.io, pero es muy posible que tarde **mucho** en estar disponible después de la creación. Yo tuve que esperar 48 horas antes de poder acceder al site. Aunque ahora las actualizaciones de contenido apenas llevan tiempo. Cuando subo un cambio el site se actualiza en cuestión de minutos.


## Problemas encontrados

* Si escribes en _Pandoc_ (básicamente lo único que cambia es la extensión: `pdc` en lugar de `md`) **no funcionan** las tablas de contenido (TOC). En cambio se renderizan correctamente los párrafos al margen de los retornos insertados en el editor de texto.

* Si escribes en _Markdown_ (ver el punto anterior) no puedes poner retornos de linea en el texto fuente (excepto para separar párrafos claro). Parece que Hugo los coserva al renderizar y el texto queda horrible con los retornos de linea insertados.
