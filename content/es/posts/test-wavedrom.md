---
title: "Soporte para Wavedrom"
date: 2019-11-18T18:00:06+09:00
description: "WaveDrom sirve para hacer diagramas de tiempo es software Libre y Abierto, el motor de renderizado usa javascript, HTML5 y SVG para convertir entradas WaveJSON en gráficos SVG."
draft: false
enableToc: false
enableTocContent: false
tags:
-
series:
-
categories:
- diagram
libraries:
- wavedrom
image: images/feature1/wave.png
---

```wave
{ 
  "signal": [ {"name": "CLK", "wave": "p.....|..."},
            {"name":"DAT", "wave":"x.345x|=.x", "data":["A","B","C","D"]},
            {"name": "REQ", "wave": "0.1..0|1.0"},
            {},
            {"name": "ACK", "wave": "1.....|01."}
]}
```
