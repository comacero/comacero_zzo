---
title: "Configuración de Emacs paso a paso (05)"
date: 2020-08-05
description: Programando en Python
draft: true
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags:
- linux
- emacs
- org-mode
categories:
- Linux
- Emacs
series:
- Emacs configuration
image: images/feature2/content.png
slug: emacs_conf_05
---

Seguimos revisando la configuración de nuestro editor _Emacs_ esta vez
vamos a ver el resto de configuraciones para programar.



## Ficheros para descargar

Los ficheros descritos: `init.el` y `myinit.org` puedes descargarlos
desde [este
enlace](https://gitlab.com/comacero/emacs_conf_step/-/archive/master/emacs_conf_step-master.zip)

Los ficheros están almacenados en
<https://gitlab.com/comacero/emacs_conf_step> e irán ampliándose a
medida que publiquemos esta serie. En realidad el único fichero que
tienes que actualizar es el `myinit.org`. El `init.el` debería cambiar
automaticamente sin intervención de nadie.

## Referencias

* [C'est la Z: Using Emacs Series](https://cestlaz.github.io/stories/emacs/)
* [Uncle Dave Emacs videolist](https://www.youtube.com/watch?v=d6iY_1aMzeg&list=PLX2044Ew-UVVv31a0-Qn3dA6Sd_-NyA1n)
* [Planet Emacs Life](https://planet.emacslife.com/)
* [use-package](https://github.com/jwiegley/use-package)
* [org-mode quickstart](https://orgmode.org/worg/org-tutorials/org4beginners.html)
* [ivy-counsel-swipper](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html)
* [emacs and python](https://github.com/howardabrams/dot-files/blob/master/emacs-python.org)
* [Yasnippet documentation](http://joaotavora.github.io/yasnippet/)
* [Magit](https://magit.vc/)
* [Documentación de elpy](https://elpy.readthedocs.io/en/latest/introduction.html)
