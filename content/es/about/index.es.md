+++
title = "About"
description = "Comacero, un blog de Linux, Software Abierto y Hardware Libre"
type = "about"
date = "20-04-13"
+++

Un blog personal para almacenar mis apuntes y "argalladas" centrado en
software y hardware libre y abierto.


