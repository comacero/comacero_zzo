+++
title = "About"
description = "Comacero, un blog de Linux, Software Aberto e Hardware Libre"
type = "about"
date = "20-04-13"
+++

Un blog persoal para almacenar os meus apuntes e "argalladas" centrado no
software e hardware libres e abertos.
