---
title: "Como se fixo este blog"
date: 2020-04-17T11:52:19+02:00
description: Explicación de como montar un site coma este con Hugo e _Gitlab Pages_
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags: 
- hugo
- gitlab
- static_site
categories:
- Linux
series:
- Guías Linux
image: images/feature2/content.png
libraries:
- mermaid
---

A receta para montar un blog coma este

<!-- more -->

Fai anos que son consciente da existencia das _Github Pages_ ou das _Gitlab Pages_. Probeinas fai moito tempo en [Github](https://github.com/) pero deixeinas por falta de tempo.

Tamén sei dende fai moito que hai xeradores de sitios web estáticos, no seu dia fixera algunhas probas con [Pelican](https://github.com/getpelican/pelican) e mais [Jekill](https://github.com/jekyll/jekyll) pero non cheguei a facer nada con eles.

Pero agora teño tempo e sentía curiosidade polo tema, asi que investiguei un pouco como podería facer para empezar rapido.

## Hugo: o xerador de sitios web estáticos


Un xerador de sitios estáticos é un software que a partir duns ficheiros de contido, e uns ficheiros de temas xera un sitio estático completo. É dicir un sitio web composto de htlm e css, nada máis. 

Os ficheiros de contido adoitan estar escritos nalgunha variedade de markdown, org-mode, ou algunha linguaxe de marcas polo estilo.

Os temas adóitan atoparse en grandes cantidades na rede, e están desenados con distintos obxectivos. Hai temas para facer a túa páxina de presentación persoal e dar a coñecer o teu  curriculum, temas para blogs, para galerias de fotos, para publicar documentación, etc. etc.

Hai gran cantidade de xeradores de sitios estáticos. Tras investigar un pouco (moi pouco, a verdade) limitei as miñas opcións a dous xeradores: [Jekill](https://jekyllrb.com/) e [Hugo](https://gohugo.io/se). Teño a sensación (totalmente subxectiva) de que Jekill é máis potente e intúo que será máis complicado de usar así que decidín empezar por Hugo.

Hugo parece sinxelo de instalar e de usar e non me deu pegas até agora.

Aquí tes [unha serie de vídeos](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) cortiños onde se explican todos os detalles de Hugo se queres aprender mais.

### Instalación de Hugo

A miña instalación é completamente espartana, descargueime o paquete binario na súa última versión (0.69.0 cando escribo isto) dende [aquí](https://github.com/gohugoio/hugo/releases) E deixei o executable `hugo` no meu directorio `~/bin`. Nada máis, ese directorio xa está no meu `PATH` así que con iso basta para min. 

{{< notice warning >}}
**Ollo** a primeira vez instalei o paquete binario _Hugo_ para Linux 64 bit, pero máis tarde o tiven que cambiar polo paquete **hugo_extended**
{{< /notice >}}

Unha vez instalado Hugo, non está de máis facer [o tutorial](https://gohugo.io/getting-started/quick-start/)

## Temas para Hugo: Zdoc y Zzo

Despois de facer o tutorial e algunhas probas din unha volta  [pola páxina de temas de Hugo](https://themes.gohugo.io/). Interesábame buscar un tema que fose axeitado para publicar guías algo extensas e que soportase publicar en varios idiomas. Por fin escollín o tema [Zdoc](https://themes.gohugo.io/hugo-theme-zdoc/) por que cumpría os meus requisitos, parecía moi ben documentado e cun aspecto gráfico que me gustaba. Pero nalgun momento despois de  entrares [na páxina do autor do tema](https://zzodocs.netlify.app) e case sen decatarme instalei o tema [zzo](https://github.com/zzossig/hugo-theme-zzo) que tampouco está nada mal e semella mesmo máis completo que o anterior (con galerias de fotos, reseñas de libros, etc.) así que seguín con este. De todos os xeitos, segue pendente probar o tema Zdoc para algunha guía longa e ver como queda.

Hai que agradecerlle a [Zzossig](https://github.com/zzossig) o currazo que se meteu creando estes temas.

### Creación de un site e instalación del tema

Para usar un tema en noso sitio o máis práctico é facer un fork do tema orixinal. Por unha banda poderemos mandarlle ao autor as nosas contribucións (no meu caso traducións e nada máis) por si lle interésa incorporalas á distribución oficial do tema. E doutra banda,  se facemos outras modificacións do tema terémolas controladas no noso fork.

Así que:

1) Creamos un fork do tema orixinal (en github no meu caso)

2) Creamos un directorio no noso ordenador, para todos os ficheiros do noso sitio estático, de aquí en diante vou supor que o noso sitio estático vaise a chamar "comacero",  ao directorio podeslle chamar como queiras,  eu vouno chamar simplemente `blog`.

````bash:
    mkdir blog
    cd blog
````

3) Clonamos o noso fork do tema elexido no directorio `blog`, aquí faremos as modificacións do tema (__axusta a url a o teu fork__) 

```bash
git clone https://github.com/zzossig/hugo-theme-zzo
```

4) Creamos o novo sitio no noso computador con Hugo. Creámolo dentro do  directorio `blog`. Podes chamalo como queiras, eu vouno a chamar igual que o sitio que queremos crear (`comacero` para este exemplo):

```bash
mkdir comacero
cd comacero
hugo new site . 

ls
archetypes  config.toml  content  data  layouts  static  themes
```

Xa temos o esqueleto do noso sitio estático, Hugo encargouse de poboalo con directorios.

Para o usar en Gitlab o noso sitio ten que ser un repositorio git:

```bash
git init
echo public > .gitignore
git add .gitignore
git commit -m "First commit"
```

{{< notice info >}} 
Cando teñamos todo preparado e xeremos o noso sitio en local con Hugo, todo o sitio estático crearase no directorio `comacero/public`. Pero __non queremos__ que ese directorio suba a Gitlab, é só para a nosa "vista-previa" do site en local. Por iso engadímolo ao ficheiro `.gitignore`
{{< /notice >}}

Temos que engadir o tema, así que clonamos **o noso fork** na ruta `themes/zzo`

{{< notice warning >}} 
Os _Gitlab Runners_, que son os procesos virtuais que van xerar o noso sitio en Gitlab, __non poden__ clonar un repo por ssh. Así que ao engadir o tema tes que facelo cunha url `https://` ou mesmo clonar directamente o directorio.
{{< /notice >}}


```bash
git submodule add https://github.com/zzossig/hugo-theme-zzo themes/zzo
git submodule update --remote --merge
```

Alternativamente **se temos o noso site e o noso fork do tema** no mesmo directorio `blog` podemos facer referencia ao submódulo cunha ruta relativa:

```bash
git submodule add ../hugo-theme-zzo themes/zzo
git submodule update --remote --merge
```

Pero se optamos pola segunda opción temos que ser cuidadosos ao subir todo a Gitlab (despois o explicamos)

Una vez que teñamos o noso tema descargado podemos copiar a configuración completa do site de exemplo que vén co tema. Dende o directorio onde temos noso site creado por Hugo, copiamos os ficheiros de configuración e de contido do site de exemplo no site que nos creamos (que está baleiro):

```bash
rm config.toml
cp -r themes/zzo/exampleSite/* .
```

E xa temos todo listo para probar, arrancamos o Hugo como servidor web:

```bash
hugo -D server
```

Se todo vai ben teriamos que poder conectarnos a `http://localhost:1313` e ver o site de exemplo.

#### Configuración do site

Co site funcionando podemos retocar os ficheiros de configuración `config.toml` e `params.toml` que están no directorio `config/_default`. Teremos que adaptar a configuración de idiomas, o nome do sitio xerado, etc. etc. na páxina web de zzo temos instrucións ben detalladas.

## Integración con Gitlab Pages

Xa comentei que mirara as páxinas de Github e de Gitlab (e tamén as de Bitbucket) hai anos. Mesmo probara algún xerador de sitios estáticos para temas do traballo. Quedei con ideas preconcibidas (e equivocadas): eu pensaba que toda a xeración do sitio facíase en local có xerador e despois subías a Gitlab o sitio xerado. É dicir, eu pensaba que despois de executar Hugo para xerar todo o sitio estático no directorio `public` subias ese contido xerado a Gitlab.

Pois para nada.

En Gitlab a xeración de sitios estáticos está completamente integrada co software Gitlab, o que permite facer [integración contínua](https://es.wikipedia.org/wiki/Integraci%C3%B3n_continua). Esta é outra materia que teño pendente así que venme moi ben para ir aprendendo. :smile:


{{< notice info >}} 
Si queres aprender como vai o despregamento de sitios estáticos con _Gitlab Pages_ é moi recomendable que leas [a documentación das Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) e que para empezar [crees un site baseado na plantilla html/plain](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_bundled_template.html) (un template predefinido por Gitlab).
{{< /notice >}} 

Os seguintes pasos serán:

* Crear o noso proxecto en Gitlab para subir os nosos ficheiros do sitio
* Subir todo o noso sitio a Gitlab excepto o contido do directorio `public`
* Crear o ficheiro que lle especifica ao _runner_ que operacións ten que facer para crear o sitio web

### Crear o noso proxecto en Gitlab

No disco duro teño unha estructura de directorios como esta:

```mermaid
graph TD;
  blog --> theme_zzo;
  blog --> comacero;
```

* No directorio `comacero` temos o noso site
* No directorio `theme_zzo` temos o fork do tema  orixinal coas nosas modificacións.

En Gitlab teño creado unha estrutura parecida:

* Un grupo que se chama `comacero`
* Un proxecto `comacero.gitlab.io` dentro do grupo `comacero` que almacenará o sitio estático, ligado ao directorio `blog/comacero` do meu disco duro
* Un proxecto `zzo_hugo_theme` que almacena o meu fork do tema orixinal, ligado ao directorio `blog/theme_zzo` do meu disco duro

{{< notice info >}} 
Convén lerse a [documentación de Gitlab](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) con respecto aos nomes de dominio e a súa correspondencia cos nomes de proxecto.
{{< /notice >}} 

### Subimos o noso site a Gitlab

No repo do directorio `blog/comacero` engadimos a url do remoto en Gitlab (`comacero.gitlab.io`) con `git remote set-url`
No repo do directorio `blog/theme_zzo` temos o fork do tema que apunta a un repo de Github. Engadimos un remoto adicional apuntando á url de Gitlab (`zzo_hugo_theme`)
Facemos un push dende os nosos repos locais hacia Gitlab e xa temos os repos preparados.

Para que o noso repo xere as páxinas web temos que engadir un ficheiro coas indicacións para o _Gitlab runner_.

### Activar o _runner_ de Gitlab

Temos que crear na raiz de noso repo `comacero.gitlab.io` un ficheiro `.gitlab-ci.yml` co seguinte contido:

```.gitlab-ci.yml
# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
# image: registry.gitlab.com/pages/hugo:latest
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

{{< notice warning >}} 
Moito ollo, porque para o noso tema necesitamos usar __hugo-extended__ e non é fácil atopar a imaxe correcta en Gitlab. Afortunadamente fixera probas co template de Gtiabl para un sitio baseado en Hugo, e se vos fixades no comentario da cabeceira do `gitlab-ci.yml` indícanos a URL na que están todas as imaxes dispoñibles de Hugo.
{{< /notice >}} 

## Final

Seguindo estes pasos deberiamos ter o noso site dispoñible na dirección https://comacero.gitlab.io, pero é moi posible que tarde **moito** en estar dispoñible despois da creación. Eu tiven que esperar **48 horas** antes de poder acceder ao site. Aínda que agora que xa está operativo, as actualizacións de contido apenas levan tempo. Cando subo un cambio o site actualízase en cuestión de minutos.


## Problemas atopados

* Se escribes en _Pandoc_ (basicamente o único que cambia é a extensión: `pdc` en lugar de `md`) **non funcionan** as táboas de contido (TOC) o resto semella ir ben, e mesmo formatea automaticamente os párrafos.

* Se escribes en _Markdown_ (ver o punto anterior) non podes pór retornos de linea no texto fonte (excepto para separar párrafos claro). Parece que Hugo respéctaos ao renderizar e o texto queda horrible cós retornos de linea insertados.
