---
title: "Configuración de Emacs paso a paso (03)"
date: 2020-06-08
description: Paquetes xerais para programar
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags:
- linux
- emacs
- org-mode
categories:
- Linux
- Emacs
series:
- Emacs configuration
image: images/feature2/content.png
slug: emacs_conf_03
---

Continuamos configurando o noso _Emacs_, esta vez imos ver paquetes útiles para programar.

Teño que comentar que eu uso _Emacs_ como cliente a maior parte do tempo. Iso fai que _Emacs_ arranque unha soa vez (por sesión) como servidor, e as sucesivas chamadas a _Emacs_ abren _buffers_ na mesma instancia.

Para traballar deste xeito creei varios alias para invocar o editor. En _zsh_ é doado, non hai máis que instalar o _bundle_ `emacs` de _oh-my_zsh_, que engade algunha funcionalidade a maiores. Pero se non usas zsh nin _oh-my-zsh_ valeríache con estes alias:

```shell
alias ef='emacsclient -create-frame --alternate-editor=""'
alias e='emacsclient --alternate-editor=""'
alias gemacs='/usr/bin/emacs'
```
Trátase de ter alias que chamen sempre a _emacsclient_, en caso de necesidade temos un alias que chama ao _Emacs_ orixinal.

Traballar desa forma é moito máis rápido e carga menos ao sistema pero tendes a acumular unha chea de ficheiros abertos (_buffers_ en _Emacs).

O meu atallo `C-del`, ou sexa control-suprimir para pechar os _buffers_ e o uso do paquete _projectile_ (`C-c p k` pecha todos os _buffers_ do proxecto activo) axúdanme a manter acoutado o número de ficheiros abertos.

## projectile

_projectile_ identifica un arbol de directorios como proxecto se é un repo de git (considera tamén outros sistemas de control de versións, pero eu só uso git) Tamén o considera proxecto si creamos un ficheiro _.projectile_ (pode estar baleiro), no directorio raiz do proxecto.

Co uso normal que fagamos do noso editor _projectile_ vai recompilando "proxectos" e apúntaos nun ficheiro no directorio `~/.emacs.d`

Coa configuración proposta temos moitísimos atajos de teclado para invocar a _projectile_, na [páxina de _counsel-projectile](https://github.com/ericdanan/counsel-projectile#getting-started) podemos ver unha ampla listaxe.

Si invocamos `C-c p` grazas a _which-key_ veremos os comandos de _projectile_ á nosa disposición. Os que máis uso son:

`C-c p p`: Cambiar de proxecto
`C-c p f`: Abrir ficheiro no proxecto actual
`C-c p b`: Ir a un _buffer_ (un ficheiro xa aberto en _Emacs_) do proxecto actual
`C-c p k`: Pechar todos os _buffers_ do proxecto actual.

Durante a miña sesión de traballo adoito cambiar a un proxecto `C-c p p` e abrir os ficheiros necesarios no proxecto que estea a traballar `C-c p f` e terminar pechando todos os ficheiros do proxecto ao final `C-c p k`

``` myinit.org
* projectile
#+begin_src emacs-lisp
  (use-package projectile
    :ensure t
    :config
    (projectile-global-mode)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
    (setq projectile-completion-system 'ivy)
    )

  (use-package counsel-projectile
    :ensure t
    :config
    (setq counsel-projectile-mode t)
    )
#+end_src
```

## git

Dous paquetes para interactuar con _git_: _Magit_ e _git-gutter_

_Magit_ é un super paquete, cunha versatilidade e utilidade á altura do _org-mode_, por si só xa xustifica usar _Emacs_. No artigo [_A walkthrought the Magit interface_](https://emacsair.me/2017/09/01/magit-walk-through/) temos una excelente introdución ao uso deste paquete que nos permite facer todas as operacións de git sen saír do noso editor. Pero non só iso, _magit_ tamén fará que si usamos _Emacs_ como editor configurado en git, úse toda a potencia de _Magit_ ao editar as mensaxes de commits.

Con `C-c g` accedemos á xanela de estado de _Magit_

Pola súa banda _git-gutter_ marcará as lineas que estamos a editar en calquera ficheiro que estea monitorizado por git, coma se tivésemos activado un diff en tempo real, ademais co arbol _hydra_ configurado podemos navegar polos cambios feitos no ficheiro e facer _commits_ de cambios atómicos(pasar ao _stage_ só os cambios no ficheiro que nos interesen) `M-g M-g` abre a _hydra_ do _git-gutter_

``` myinit.org
* git
#+begin_src emacs-lisp
  (use-package magit
    :ensure t
    :init
    (progn
      (bind-key "C-x g" 'magit-status)
      )
    )

  (setq magit-status-margin
        '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 18))

  (use-package git-gutter
    :ensure t
    :init
    (global-git-gutter-mode +1)
    )

  (global-set-key (kbd "M-g M-g") 'hydra-git-gutter/body)

  (use-package git-timemachine
    :ensure t
    )

  (defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1)
                              :hint nil)
    "
    Git gutter:
      _j_: next hunk        _s_tage hunk     _q_uit
      _k_: previous hunk    _r_evert hunk    _Q_uit and deactivate git-gutter
      ^ ^                   _p_opup hunk
      _h_: first hunk
      _l_: last hunk        set start _R_evision
    "
    ("j" git-gutter:next-hunk)
    ("k" git-gutter:previous-hunk)
    ("h" (progn (goto-char (point-min))
                (git-gutter:next-hunk 1)))
    ("l" (progn (goto-char (point-min))
                (git-gutter:previous-hunk 1)))
    ("s" git-gutter:stage-hunk)
    ("r" git-gutter:revert-hunk)
    ("p" git-gutter:popup-hunk)
    ("R" git-gutter:set-start-revision)
    ("q" nil :color blue)
    ("Q" (progn (git-gutter-mode -1)
                ;; git-gutter-fringe doesn't seem to
                ;; clear the markup right away
                (sit-for 0.1)
                (git-gutter:clear))
     :color blue))
#+end_src
```

## yasnippet

## yasnippet

_yasnippet_ permítenos usar plantillas para bloques moi usados. Por exemplo si estamos a programar, digamos en python, podemos ter plantillas para os bloques _for_ ou para un bloque _if-else_  ou para a cabeceira de ficheiros, etc. Xunto con _yasnippet_ instalei _yasnippets-snippets_ que é una boa colección de _snippets_ predefinidos.

Podes crear facilmente os teus propios _snippets_ (bota un ollo á documentación)

Como complemento instalo tamén o paquete _auto-yasnippet_ que permite crear un _snippet_ "ao voo" mentres estás a editar.

Na sección _own-map_ engadimos un mapa de teclado con atallos propios. Ahí temos asignado `C-ñ y` Crear novo _auto-snippet_ y `C-ñ e` Expandir o _auto-yasnippet_. Se non quedas convencido coa 'ñ' podes usar calqueira outra letra libre, por exemplo `C-z`

```myinit.org
* yasnippet
Place your own snippets on ~/.emacs.d/snippets
  #+begin_src emacs-lisp
    (use-package yasnippet
      :ensure t
      :config
      :init
      (yas-global-mode 1)
      )
    (use-package auto-yasnippet
      :ensure t
      )
    (use-package yasnippet-snippets
      :ensure t)
  #+end_src
```

## flycheck

_flycheck_ daranos detección de erros sintácticos cando estemos programando.

```myinit.org
* flycheck
  #+begin_src emacs-lisp
    (use-package flycheck
      :ensure t
      :config
      (global-set-key  (kbd "C-c C-p") 'flycheck-previous-error)
      (global-set-key  (kbd "C-c C-n") 'flycheck-next-error)
      :init
      (global-flycheck-mode t)
      )
  #+end_src
```

## auto-complete o company

Hai dúas escolas principais de auto completado en _Emacs_: podemos usar o paquete _auto-complete_ ou o paquete _company_. Eu ata agora usei _auto-complete_, pero estou a probar _company_ que parece que dá menos problemas de compatibilidade cos paquetes que uso para programar en python.

Os ficheiros de configuración baseados en _auto-complete_ quedan no repositorio de gitlab (ver sección [ficheiros para descargar](#ficheiros-para-descargar)) na rama _*auto-complete*_. Useinos durante varios meses sen demasiados problemas. Para o resto desta serie imos utilizar unha configuración baseada en _company_.

# company

_company_ é un paquete moi modular que se complementa con _plugins para distintas tarefas, a medida que vaiamos engadindo novas seccións para distintas contornas de programación iremos engadindo os _plugins_ necesarios.

De momento imos engadir a configuración básica de _company_:

```myinit.org
* company
  Autocompletion with company
  #+begin_src emacs-lisp
    (use-package company
      :ensure t
      :config
      (setq company-idle-delay 0)
      (setq company-minimum-prefix-length 3)
      (global-company-mode t)
      )
  #+end_src
```

## Ficheiros para descargar

Os ficheiros descritos: `init.el` e `myinit.org` podes descargalos desde [este enlace](https://gitlab.com/comacero/emacs_conf_step/-/arquive/master/emacs_conf_step-master.zip)

Os ficheiros están almacenados en  <https://gitlab.com/comacero/emacs_conf_step> e iranse ampliándo a medida que publiquemos a serie. En realidade o único ficheiro que tes que actualizar é o `myinit.org`. O `init.el` debería cambiar automaticamente sen intervención de ninguén.

## Referencias

* [C'est la Z: Using Emacs Series](https://cestlaz.github.io/stories/emacs/)
* [Uncle Dave Emacs videolist](https://www.youtube.com/watch?v=d6iY_1aMzeg&list=PLX2044Ew-UVVv31a0-Qn3dA6Sd_-NyA1n)
* [Planet Emacs Life](https://planet.emacslife.com/)
* [use-package](https://github.com/jwiegley/use-package)
* [org-mode quickstart](https://orgmode.org/worg/org-tutorials/org4beginners.html)
* [ivy-counsel-swipper](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html)
* [Yasnippet documentation](http://joaotavora.github.io/yasnippet/)
* [Magit](https://magit.vc/)