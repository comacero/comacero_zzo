---
title: "Configuración de Emacs paso a paso (02)"
date: 2020-05-29
description: Continúa a serie "Configuración de Emacs paso a paso"
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags:
- linux
- emacs
- org-mode
categories:
- Linux
- Emacs
series:
- Emacs configuration
image: images/feature2/content.png
slug: emacs_conf_02
---

Neste artigo imos continuar configurando o noso _Emacs_ engadindo uns poucos paquetes útiles á nosa configuración. Engadiremos unha serie de paquetes para facer máis doado o acceso a funcións básicas do editor e por último instalaremos un tema gráfico. É dicir un ficheiro que configura o aspecto visual do noso editor.

## ace-windows

_Emacs_ é un software que se inventou bastante antes do concepto de xanelas que manexamos nas contornas gráficas de todos os sistemas de escritorio. Por esa razón usa unha terminoloxía algo arcaica. As xanelas que coñecemos en calquera contorna gráfica chámanse _frames_ en _Emacs_ e as seccións nas que podemos dividir unha xanela do editor chámanse _windows_.

Para que quede claro podes probar a dividir a túa pantalla de _Emacs_ en varias xanelas usando `C-x 2` ou `C-x 3` unhas cantas veces. Se o que queres é abrir un novo _frame_ o comando sería `C-x 4 f`

_ace-windows_ é un paquete que nos axuda cando temos varias _windows_ de _Emacs_ e queremos cambiar de xanela co comando `C-x ou`. Todas as xanelas abertas mostrarán un número e non temos máis que teclear o número elixido para saltar á que queiramos.


``` org {linenos=false}
* ace-windows
  To jump to different windows easily
  #+begin_src emacs-lisp
  (use-package ace-window
    :ensure t
    :init
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    )
  #+end_src
```

## which-key

_which-key_ é un paquete que nos facilita a vida ao teclear comandos. Proba a teclear `C-x` (o prefixo de moitísimos comandos no noso editor) antes e despois de instalar o paquete para ver a diferenza.

* _which-key_ permítenos ir construíndo o comando aos poucos, podemos borrar o último caracter introducido con `C-h u`
* Se temos varias páxinas de posibles combinacións podemos usar `C-h n` e `C-h p` para movernos á seguinte (_next_) ou anterior (_prior_)
* Se pousamos o cursor do rato encima dalgunha das suxestións que nos aparecen no _minibuffer_ veremos un texto de documentación do comando.
* `C-h a` aborta o comando

_which-key_ funcionará mesmo cos atallos de teclado que nós configuremos. En cambio non funcionará con `M-x` (_execute-extended-command_) e é boa cousa que non o faga. Calcúlase que _Emacs_ é capaz de executar uns dezmil comandos e iso antes de que instalemos ningún paquete opcional. Sería complicado usar _which-key_ con esa cantidade. De todos os xeitos no seguinte apartado instalaremos un paquete que nos axudará con iso tamén.

``` org {linenos=false}
* which-key
  Some help with hotkeys
  #+begin_src emacs-lisp
    (use-package which-key
      ;; give us help for keys in RT
      :ensure t
      :config (which-key-mode))
  #+end_src
```

## ivy-counsel-swipper

Imos instalar tres paquetes que traballan en equipo e melloran moitísimo a experiencia de usuario en _Emacs_.

_ivy_ é un xestor de listas. A lista xestionada pode ser de case calquera cousa. Podería ser unha lista de ficheiros se invocamos a función de cargar ficheiro. Ou podería ser unha lista de coincidencias se estamos a buscar unha palabra no editor, ou unha lista de comandos se estamos tecleando un comando no _minibuffer_.

_ivy_ por si só sería un pouco difícil de usar, así que se instalan tamén _swipper_ que implementa un interfaz de procuras mellorado dentro do ficheiro que estamos a editar, e _counsel_ que engade __moitas__ funcionalidades apoiándose en _ivy_. En realidade se instalamos _counsel_ os outros dous paquetes veñen como dependencias.

Unha vez instalados os paquetes non temos máis que teclear `M-x` para velos en acción. Se vos fixades _ivy_ engade un caracter '^' á cadea de procura. Ese caracter significa "principio da cadea" en _regexp_, así que si tecleo _window_, _ivy_ filtrará todos os comandos __que empecen__ por _window_. Se borro o '^' e tecleo window, entón _ivy_ devolve todos os comandos __que conteñan__ a cadea _window_. Xa vedes que coa funcionalidade combinada de _counsel_ e _ivy_ podo buscar comandos no _minibuffer_ de `M-x` sen saber exactamente como se chaman.

``` org {linenos=false}
* ivy - counsel - swipper
  An enhanced incremental search and a lot of features from counsel
  #+begin_src emacs-lisp
    (use-package counsel
      :ensure t
      :bind
      (("C-S-o" . counsel-rhythmbox)
       ("M-x" . counsel-M-x)
       ("C-x C-f" . counsel-find-file)
       ("<f1> f" . counsel-describe-function)
       ("<f1> v" . counsel-describe-variable)
       ("<f1> l" . counsel-load-library)
       ("<f2> i" . counsel-info-lookup-symbol)
       ("<f2> u" . counsel-unicode-char)
       ("C-c g" . counsel-git)
       ("C-c j" . counsel-git-grep)
       ;; ("C-c k" . counsel-ag)    requires installation: apt install silversearcher-ag
       ("C-x l" . counsel-locate)
       ("M-y" . counsel-yank-pop)
       :map ivy-minibuffer-map
       ("M-y" . ivy-next-line))
      )
    ;; Takes care of elections menus
    (use-package ivy
      :ensure t
      :diminish (ivy-mode)
      :bind
      (("C-x b" . ivy-switch-buffer)
       ("<f6>" . ivy-resume)
      )
      :config
      (ivy-mode 1)
      (setq ivy-use-virtual-buffers t
            ivy-count-format "%d/%d "
            ivy-display-style 'fancy)
      )

    (use-package swiper
      :ensure t
      :bind
      (("C-s" . swiper-isearch)
       ("C-r" . swiper-isearch-backward)
       :map minibuffer-local-map
       ("C-r" . counsel-minibuffer-history)
      )
      :config
      (ivy-mode 1)
      (setq ivy-use-virtual-buffers t)
      )
  #+end_src
```

Paga a pena ler os manuais dos paquetes e [este artigo](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html) tamén está moi ben.

Algo que me despistou un pouco ao principio foi o diálogo de abrir ficheiros `C-x C-f`. Ás veces o autocompletado de _ivy_ pódenos interferir, por exemplo se queremos abrir un novo ficheiro chamado _test_ e nese directorio xa existe _test.txt_, o autocompletado sempre nos abrirá o segundo en lugar de crear o primeiro. O problema resólvese tecleando `C-M-j` (_ivy-inmediate-do_) para executar a acción de abrir ficheiro xusto coa cadea que temos tecleado e non co autocompletado.

Se non che convencen estos paquetes, dis que o paquete _Helm_ é unha alternativa á combinación destes tres, pero eu nunca o probei.

## hydra

_hydra_ permite montar "árbores de comandos" na configuración proposta veñen tres árbores configuradas: unha de comandos que conmutan entre dous estados, unha de comandos de navegación, e outra de comandos para _org-mode_

Mais adiante usaremos _hydra_ para facer novas árbores.

``` org {linenos=false}
* hydra
Defines command trees (so to speak)
#+begin_src emacs-lisp
  (use-package hydra
    :ensure t)
#+end_src

** Hydra for toggle modes
#+begin_src emacs-lisp
  (global-set-key
   (kbd "C-x t")
   (defhydra toggle (:color blue)
     "toggle"
     ("a" abbrev-mode "abbrev")
     ("s" flyspell-mode "flyspell")
     ("d" toggle-debug-on-error "debug")
     ("c" fci-mode "fCi")
     ("f" auto-fill-mode "fill")
     ("t" toggle-truncate-lines "truncate")
     ("w" whitespace-mode "whitespace")
     ("q" nil "cancel")
     )
   )
#+end_src

** Hydra for navigation
#+begin_src emacs-lisp
  (global-set-key
   (kbd "C-x j")
   (defhydra gotoline
     (:pre (linum-mode 1)
      :post (linum-mode -1))
     "goto"
     ("t" (lambda () (interactive)(move-to-window-line-top-bottom 0)) "top")
     ("b" (lambda () (interactive)(move-to-window-line-top-bottom -1)) "bottom")
     ("m" (lambda () (interactive)(move-to-window-line-top-bottom)) "middle")
     ("e" (lambda () (interactive)(end-of-buffer)) "end")
     ("c" recenter-top-bottom "recenter")
     ("n" next-line "down")
     ("p" (lambda () (interactive) (forward-line -1))  "up")
     ("g" goto-line "goto-line")
     )
   )
#+end_src

** Hydra for some org-mode stuff
#+begin_src emacs-lisp
  (global-set-key
   (kbd "C-c t")
   (defhydra hydra-global-org (:color blue)
     "Org"
     ("t" org-timer-start "Start Timer")
     ("s" org-timer-stop "Stop Timer")
     ("r" org-timer-set-timer "Set Timer") ; This one requires you be in an orgmode doc, as it sets the timer for the header
     ("p" org-timer "Print Timer") ; output timer value to buffer
     ("w" (org-clock-in '(4)) "Clock-In") ; used with (org-clock-persistence-insinuate) (setq org-clock-persist t)
     ("o" org-clock-out "Clock-Out") ; you might also want (setq org-log-note-clock-out t)
     ("j" org-clock-goto "Clock Goto") ; global visit the clocked task
     ("c" org-capture "Capture") ; Don't forget to define the captures you want http://orgmode.org/manual/Capture.html
     ("l" (or )rg-capture-goto-last-stored "Last Capture")
     )
   )
#+end_src
```

## avy

_avy_ é un paquete que permite moverse rapidamente polo ficheiro editado sen usar o rato. A configuración que ves aquí permíteche teclear dous caracteres e marcará todas as coincidencias no ficheiro cada unha indicada cunha letra-código diferente. Podes moverte a calquera coincidencia sen mais que teclear o seu código. A verdade é que non a uso moito.

``` org {linenos=false}
* avy
  #+begin_src emacs-lisp
    ;; avy
    (use-package avy
      :ensure t
      :config
      (avy-setup-default)
      :bind(
            ("M-g c" . avy-goto-char-2)
            )
      )
  #+end_src
```

## iedit

_iedit_ é moito máis interesante, se entramos no _iedit-mode_ pulsando `C-;` _iedit_ seleccionará todas as ocorrencias, en todo o ficheiro, do símbolo baixo o cursor (ou do que teñamos selecionado na rexión se está activa) e permitiranos editar todas as ocorrencias dunha soa vez. Con _iedit_ activado a combinación `M-h` restrínxe a edición as ocurrencias na función actual (moi útil si estamos a programar). Hai moitas mais formas de restrición, o mellor é ler a descrición do modo (`C-h m iedit-mode`). Cando terminemos a edición pulsamos de novo `C-;`

``` org {linenos=false}
* iedit
Interactive edition of all ocurrences of X
  #+begin_src emacs-lisp
    (use-package iedit
      :ensure t)
  #+end_src
```

## color-theme

Aínda non traballei moito nos temas gráficos, teño configurado o _leuven_theme_ e parece que fai un bo traballo. Os ficheiros _org-mode_ vense ben e non teño ningún problema editando texto ou programando.

Aínda non teño ningún tema _dark_ ben probado. E tamén teño pendentes investigar os temas _doom_, se os dou probado terán un artigo dentro desta serie.

``` org {linenos=false}
(use-package color-theme-modern
  :ensure t)
(use-package leuven-theme
  :ensure t
  ;; :config
  ;; (load-theme 'leuven-dark t)
  ;; (setq leuven-scale-outline-headlines t)
  ;; (setq leuven-scale-org-agenda-structure nil)
  )
```

## Ficheiros para descargar

Os ficheiros descritos: `init.el` e `myinit.org` podes descargalos desde [este enlace](https://gitlab.com/comacero/emacs_conf_step/-/arquive/master/emacs_conf_step-master.zip)

Os ficheiros están almacenados en  <https://gitlab.com/comacero/emacs_conf_step> e iranse ampliándo a medida que publiquemos a serie. En realidade o único ficheiro que tes que actualizar é o `myinit.org`. O `init.el` debería cambiar automaticamente sen intervención de ninguén.

## Referencias

* [C'est la Z: Using Emacs Series](https://cestlaz.github.io/stories/emacs/)
* [Uncle Dave Emacs videolist](https://www.youtube.com/watch?v=d6iY_1aMzeg&list=PLX2044Ew-UVVv31a0-Qn3dA6Sd_-NyA1n)
* [Planet Emacs Life](https://planet.emacslife.com/)
* [use-package](https://github.com/jwiegley/use-package)
* [org-mode quickstart](https://orgmode.org/worg/org-tutorials/org4beginners.html)
* [ivy-counsel-swipper](https://writequit.org/denver-emacs/presentations/2017-04-11-ivy.html)
