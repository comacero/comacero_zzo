---
title: "Actualizar git en Linux Mint Tricia"
date: 2020-04-21T11:52:19+02:00
description: Actualiza git para evitar os problemas de seguridade detectados nas últimas semanas
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags: 
- linux
- mint
- git
categories:
- Linux
- git
series:
- Tips for linux
image: images/feature2/content.png
---

## Actualización de git en Linux Mint

git tivo unha chea de actualizacións nas últimas semanas debido a problemas de seguridade.

Para instalar a última versión en Linux Mint Tricia (válido tamén para Ubuntu 18.04) usei o  ppa dos desarrolladores.

``` bash {linenos=false}
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt upgrade
```

Con esto pasei de git 2.17.1 a la 2.26.2.
