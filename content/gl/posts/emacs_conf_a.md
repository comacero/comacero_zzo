---
title: "Configuración de Emacs paso a paso (01)"
date: 2020-05-25
description: Primer artículo de la serie "Configuración de Emacs paso a paso"
draft: false
hideToc: true
enableToc: true
enableTocContent: true
tocPosition: inner
tags:
- linux
- emacs
- org-mode
categories:
- Linux
- Emacs
series:
- Emacs configuration
image: images/feature2/content.png
slug: emacs_conf_01
---

Nesta serie de artigos describirei a miña configuración do editor _Emacs_ (concretamente GNU Emacs) paso a paso.

Levo anos usando _Emacs_ no traballo, pero sen profundar demasiado no seu estudo. Cando tiña unha necesidade buscaba unha solución na rede e retocaba o meu ficheiro de configuración a fume de carozo. Funcionaba, pero a miña configuración era un pastiche @infumable.

Grazas ás referencias que poño ao final do artigo, e en especial á serie de vídeos do señor Zamansky ([C'est la Z](https://cestlaz.github.io/stories/emacs/)) organicei o meu ficheiro de configuración e engadín bastantes cousas que me non coñecía (mágoa non telas sabido antes)

Neste primeiro artigo deixaremos lista unha configuración básica separada en dous ficheiros. O ficheiro `~/.emacs.d/init.el` que seguramente non teremos que volver a tocar, xa que faremos toda a configuración no ficheiro `~/.emacs.d/myinit.org`.

## Avisos

Si xa estás a usar _Emacs_ fai un backup da túa configuración antes de enredar con ela. Ou mellor aínda, [controla os teus ficheiros de configuración con git](https://gitlab.com/salvari/LinuxMint_Tricia_19.3#controlar-dotfiles-con-git).

Só uso Linux, antes usaba Windows no traballo pero agora uso Linux para todo, si usas Windows ou Mac é posible que teñas que adaptar cousas para o teu S.O..

## Ficheiros de configuración

A proposta é controlar toda a configuración do noso editor con dous ficheiros:

`~/.emacs.d/init.o`
: Contén a configuración mínima de _Emacs_: as seccións que _Emacs_ completa automáticamente, a declaración das fontes de paquetes e a configuración inicial da xestión de paquetes do noso editor.

`~/.emacs.d/myinit.org`
: Contén toda a nosa configuración organizada nun ficheiro _org-mode_
<br>

Si temos o noso editor _Emacs_ recentemente instalado non teremos ningún ficheiro de configuración no noso directorio `$HOME`.

{{< notice info >}}
Se non queres seguir o artigo paso a paso __e non tes nada configurado no teu Emacs__ podes ir á sección [Ficheiros para descargar](#ficheiros-para-descargar) e asegurarte de deixar os ficheiros `init.el` e `myinit.org` no directorio `~/.emacs.d`.
__Ollo!__ Se fas esto perderás toda a tua configuración previa.
{{< /notice >}}

Podemos facer un pequeno cambio de configuración e salvar as nosas opcións:

* Menú: __Options → Set Default Font__: Aumentamos o tamaño do tipo de fonte a 14 puntos por exemplo
* Menu: __Options → Save Options__: Salvamos as nosas opcións

_Emacs_ salvará as opcións no ficheiro `~/.emacs` que terá o seguinte contido:

```init.el {linenos=false}
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 143 :width normal)))))
```

Este é o noso primeiro ficheiro de configuración. O ficheiro está escrito en elisp, unha variante da linguaxe LISP que usa _Emacs_.

__Non debemos__ tocar as dúas seccións que aparecen no ficheiro, estas seccións manipúlaas automaticamente o editor e non deberiamos enredar con elas sen saber moi ben o que facemos. A pesar desta advertencia voute propor un cambio na sección `custom-set-faces` para o noso ficheiro `init.o` definitivo. Na miña instalación de _Emacs_ non se resaltaba a _region_ (o texto selecionado en _Emacs_ chámase _region_ en inglés) Se che pása o mesmo propóñoche que cambies a sección `custom-set-faces` pola seguinte:

```custom-set-faces.el {linenos=false}
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 143 :width normal))))
 '(region ((t (:background "light sea green" :distant-foreground "gtk_selection_fg_color")))))
```

_Emacs_ pode ler a súa configuración inicial dende o ficheiro `~/.emacs` e alternativamente pode usar o ficheiro `~/.emacs.d/init.el`. No directorio `~/.emacs.d/` imos ter outros ficheiros a medida que vaiamos completando a configuración do noso editor. A min paréceme máis limpo ter toda a configuración de _Emacs_ neste directorio. Así que simplemente movemos/renomeamos o noso ficheiro `~/.emacs` á nova localización executando:

```shell
cd
mv .emacs .emacs.d/init.el
```

Pechamos o noso editor _Emacs_ e abrímolo de novo para comprobar que le correctamente o ficheiro de configuración `~/.emacs.d/init.el` (deberiamos ver o tipo de letra de 14 puntos)

## Ficheiros de configuración propostos

### myinit.org

Antes de nada imos crear o ficheiro `~/.emacs.d/myinit.org` para que non de erro a carga do `init.el`. Crearémolo cun contido mínimo:

```myinit.org {hl_lines=[6],linenos=false}
#+startup: overview

* Interface tweaks
** Some GUI optimizations
#+begin_src emacs-lisp
  (setq inhibit-startup-message t) ; Eliminate FSF startup msg
#+end_src

```

O ficheiro só ten una linea de configuración para _Emacs_ (a linea resaltada) que inhibe a pantalla de arranque do editor.

Este ficheiro está escrito en _org-mode_, facémolo así por que a medida que avancemos verás que queda moito máis limpo e fácil de entender. Nesta serie non imos entrar en detalles de _org-mode_ pero podes investigar pola túa conta. _org-mode_ é unha ferramenta potentísima e por si só xa sería una boa razón para usar _Emacs_

### init.el

Agora modificamos o noso ficheiro `~/.emacs.d/init.el`. Imos engadir dúas seccións: unha que configura a instalación de paquetes e outra sección que se encarga de ler o resto da configuración desde o ficheiro `myinit.org`.

O ficheiro `init.el` definitivo é o seguinte:

```init.el
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 143 :width normal))))
 '(region ((t (:background "light sea green" :distant-foreground "gtk_selection_fg_color")))))

;;----------------------------------------------------------------------
;; Package management
;; MELPA and others
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("gnu" . "http://elpa.gnu.org/packages/") t)
;;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)
(setq package-initialize-at-startup nil)
(package-initialize)

;; Set use-package install
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;------------------------------------------------------------
;; Load org-configuration file
(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))
```

Lembra que as duas primeiras seccións son para uso privado do _Emacs_

Nas lineas 18-25 configuramos os depósitos de paquetes para _Emacs_ en internet. Hai tres depósitos principais de paquetes para _Emacs_:

GNU Elpa
: O depósito oficial de _Emacs_

Melpa
: Un depósito alternativo con moitos máis paquetes que o oficial, e
  con actualizacións máis frecuentes

Marmalade
: Era outro depósito alternativo, pero está inactivo desde hai tempo.
<br>


Nas lineas 28-30 instalamos (si non está instalado previamente) o noso primeiro paquete para _Emacs_: `use-package`.

[use-package](https://github.com/jwiegley/use-package) é un paquete para xestionar a instación e configuración de paquetes en _Emacs_, una verdadeira xoia. Os ficheiros de configuración quedan simplificados, limpos e claros.

Por último na linea 34 cargamos o noso ficheiro `myinit.org`.

Con isto terminamos de configurar o noso ficheiro `~/.emacs.d/init.el`, é posible que non teñamos que volver tocalo nunca.

Agora si detemos o noso _Emacs_ e abrimos de novo veremos (abaixo de todo, na linea de estado) que _Emacs_ se conécta a internet, descarga o paquete `use-package` e procede a instalalo.

{{< notice info >}}
Si volvemos abrir o ficheiro `init.el` veremos que _Emacs_ mantén unha lista cos paquetes instalados. __Non debemos editala__
{{< /notice >}}

Dependendo da versión de _Emacs_ que esteas a utilizar poida que vexas un erro de GPG por non poder verificar a firma dos paquetes de Elpa. De feito ningún paquete de Elpa estará dispoñible para instalación. __Solucionarémolo na seguinte sección.__

## Ampliando a configuración

Imos completar a configuración inicial no ficheiro `~/.emacs.d/myinit.org`

Neste paso imos introducir moitos cambios de golpe. Non imos detallar todos, podes ver os comentarios no propio ficheiro e eliminar (ou comentar) os cambios que non che convenzan.

Xa comentamos que o ficheiro está escrito usando _org-mode_. As seccións en _org-mode_ márcanse con un ou varios asteriscos e pódese pregar e despregar en _Emacs_ pulsando tabulador no título da sección.

A primeira linea (linea 1) do ficheiro é un _preamble_ de _org_mode_ para indicar como debe presentar as seccións despregadas ao abrir o ficheiro.

Do resto de lineas, o único que _Emacs_ interpretará como comandos de configuración son as partes encerradas entre indicadores *begin_src* e *end_src* o resto, os títulos de sección e os textos engadidos, son só para organizar o ficheiro de configuración e dar aclaracións.

As primeiras seccións até a linea 52 fan axustes variados ao interfaz de usuario, fixan a codificación preferida a `utf-8`, establecen algúns atallos de teclado útiles (por exemplo esc-esc-c cargará este ficheiro `myinit.org` no editor), e fan que o realzado de sintaxe estea activado por defecto.

A partir de aí definimos tres funciones en elisp e asignámoslles un atallo de teclado.

_Kill Buffer Quick_ das lineas 53 á 64 define unha función para matar o buffer (ficheiro) que estamos a editar sen ter que percorrer todo o diálogo por defecto en _Emacs_ e asóciao a Ctrl-Supr

_Wonderful Bubble Buffer_ Mantén unha listaxe de ficheiros recentes polo que podemos movernos coa tecla F8 (cando visitamos un buffer ascende na lista como unha burbulla) Sempre podes moverche normalmente polos buffers abertos con Shift-PgUp e Shift-PgDown que definimos na sección de _Shortcuts_

_parent matching with %_ Permítenos saltar ao paréntesis asociado co que sinale o cursor sen máis que pulsar % (imitando a outros editores como Vim)


A continuación temos una seccion dedicada a _use-package_ nesta sección solucionamos o erro das novas claves GPG de Elpa instalando un paquete coas novas claves (para o que desactivamos un momento a verificación de claves). E instalamos outro paquete que nos permitirá especificar no futuro si hai paquetes de sistema que sexan dependencias necesarias para as nosas ampliacións de _Emacs_. Ademais instalamos _try_ para poder probar paquetes _Emacs_ sen tocar a configuración nin afectala coa proba.

Por último, e adiantándonos un pouco aos acontecementos, engadimos unha sección para a configuración de org-mode e instalamos _org-bullets_, unha vez instalado este paquete verás que o ficheiro `myinit.org` vese doutra maneira en _Emacs_.

```myinit.org
#+startup: overview

* Interface tweaks
** Some GUI optimizations
   #+begin_src emacs-lisp
     (setq inhibit-startup-message t) ; Eliminate FSF startup msg
     (setq frame-title-format "%b")   ; Put filename in titlebar
     ;; (setq visible-bell t)            ; Flash instead of beep
     (set-scroll-bar-mode 'right)     ; Scrollbar placement
     (show-paren-mode t)              ; Blinking cursor shows matching parentheses
     (electric-pair-mode t)           ; electric-pair-mode on
     (setq column-number-mode t)      ; Show column number of current cursor location
     (mouse-wheel-mode t)             ; wheel-mouse support
     (setq fill-column 78)
     (setq auto-fill-mode t)                   ; Set line width to 78 columns...
     (setq-default indent-tabs-mode nil)       ; Insert spaces instead of tabs
     (global-set-key "\r" 'newline-and-indent) ; turn autoindenting on
     ;(set-default 'truncate-lines t)          ; Truncate lines for all buffers
   #+end_src

** Set encoding
   Use utf-8 please
   #+begin_src emacs-lisp
     ;; Set encoding
     (prefer-coding-system 'utf-8)
   #+end_src

** Some shortcuts
   Useful shortcuts
   #+begin_src emacs-lisp
     (global-set-key (kbd "\e\ec")
                     (lambda () (interactive) (find-file "~/.emacs.d/myinit.org"))
                     )

     (global-set-key [C-tab] 'hippie-expand)                    ; expand
     (global-set-key [C-kp-subtract] 'undo)                     ; [Undo]
     (global-set-key [C-kp-multiply] 'goto-line)                ; goto line
     (global-set-key [C-kp-add] 'toggle-truncate-lines)         ; truncate lines
     (global-set-key [C-kp-divide] 'delete-trailing-whitespace) ; delete trailing whitespace
     (global-set-key [C-kp-decimal] 'completion-at-point)       ; complete at point
     (global-set-key [C-M-prior] 'previous-buffer)              ; previous-buffer
     (global-set-key [C-M-next] 'next-buffer)                   ; next-buffer
   #+end_src

** Syntax highlight
   Set maximum colors
   #+begin_src emacs-lisp
     (cond ((fboundp 'global-font-lock-mode)        ; Turn on font-lock (syntax highlighting)
            (global-font-lock-mode t)               ; in all modes that support it
            (setq font-lock-maximum-decoration t))) ; Maximum colors
   #+end_src

** Kill buffer quick
   Kill current buffer without questions
   #+begin_src emacs-lisp
     ;;------------------------------------------------------------
     ;; Kill current buffer with C-Supr
     (defun geosoft-kill-buffer ()
       ;; Kill default buffer without the extra emacs questions
       (interactive)
       (kill-buffer (buffer-name))
       (set-name))
     (global-set-key [C-delete] 'geosoft-kill-buffer)
   #+end_src

** Wonderful bubble-buffer
   Switch to most recent buffers with F8
   #+begin_src emacs-lisp
     ;;------------------------------------------------------------
     ;; The wonderful bubble-buffer
     (defvar LIMIT 1)
     (defvar time 0)
     (defvar mylist nil)
     (defun time-now ()
     (car (cdr (current-time))))
     (defun bubble-buffer ()
     (interactive)
     (if (or (> (- (time-now) time) LIMIT) (null mylist))
         (progn (setq mylist (copy-alist (buffer-list)))
                (delq (get-buffer " *Minibuf-0*") mylist)
                (delq (get-buffer " *Minibuf-1*") mylist)))
     (bury-buffer (car mylist))
     (setq mylist (cdr mylist))
     (setq newtop (car mylist))
     (switch-to-buffer (car mylist))
     (setq rest (cdr (copy-alist mylist)))
     (while rest
       (bury-buffer (car rest))
       (setq rest (cdr rest)))
     (setq time (time-now)))
     (global-set-key [f8] 'bubble-buffer)
   #+end_src

** parent matching with %
   Jump to asociated parent with '%' same as other editors
   #+begin_src emacs-lisp
     ;;------------------------------------------------------------
     ;; Use % to match various kinds of brackets...
     ;; See: http://www.lifl.fr/~hodique/uploads/Perso/patches.el
     (global-set-key "%" 'match-paren)   ; % key match parents
     (defun match-paren (arg)
       "Go to the matching paren if on a paren; otherwise insert %."
       (interactive "p")
       (let ((prev-char (char-to-string (preceding-char)))
             (next-char (char-to-string (following-char))))
         (cond ((string-match "[[{(<]" next-char) (forward-sexp 1))
               ((string-match "[\]})>]" prev-char) (backward-sexp 1))
               (t (self-insert-command (or arg 1))))))
   #+end_src

* use-package
** use-package tips
   Some notes about *use-package*
   - *:ensure* if true will install the package if not installed
     It won't update packages. See auto-package-update for keeping all
     packages up to date
   - *:init* keyword to execute code before a package is loaded. It
     accepts one or more foorms, up to the next keyword
   - *:config* can be used to execute code after a package is loaded.
     In cases where loading is done lazily (see more about autoloading
     below), this execution is deferred until after the autoload
     occurs
   - *:bind*
   - *bind-keymap*
   - *:mode* and *:interpreter*
   - *:magic*
   - *:hook*
   - *:if*

** helpers
   =gnu-elpa-keyring-update= Avoids GPG errors in emacs version prior
   to 26.3 And =use-package-ensure-system-package= gives a warning
   when needed system packages are missing
   #+begin_src emacs-lisp
     (unless (package-installed-p 'gnu-elpa-keyring)
       (setq package-check-signature nil)
       (use-package gnu-elpa-keyring-update
         :ensure t)
       (setq package-check-signature 'allow-unsigned))
     (use-package use-package-ensure-system-package
       :ensure t)
   #+end_src

** try
   To try package without definitive installation
   #+begin_src emacs-lisp
     (use-package try
       ;; for trying packages without installing
       :ensure t)
   #+end_src

* org-mode
** org-bullets
   Nice bullets for org-mode
   #+begin_src emacs-lisp
     (use-package org-bullets
       :ensure t
       :config
       (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
       )
   #+end_src
```

## Ficheiros para descargar

Os ficheiros descritos: `init.el` e `myinit.org` podes descargalos desde [este enlace](https://gitlab.com/comacero/emacs_conf_step/-/arquive/master/emacs_conf_step-master.zip)

Os ficheiros están almacenados en <https://gitlab.com/comacero/emacs_conf_step> e irán ampliándose a medida que publiquemos a serie.

## Referencias

* [C'est la Z: Using Emacs Series](https://cestlaz.github.io/stories/emacs/)
* [Uncle Dave Emacs videolist](https://www.youtube.com/watch?v=d6iY_1aMzeg&list=PLX2044Ew-UVVv31a0-Qn3dA6Sd_-NyA1n)
* [Planet Emacs Life](https://planet.emacslife.com/)
* [use-package](https://github.com/jwiegley/use-package)
* [org-mode quickstart](https://orgmode.org/worg/org-tutorials/org4beginners.html)
